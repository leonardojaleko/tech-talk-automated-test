<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest; 
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest{
    public function authorize(){
        return true;
    }

    public function rules(){ 
        return [
            'name' => 'required|min:4',
            'email' => ['required', Rule::unique('users')->ignore(request()->route()->parameter('id'))],
            'password' => 'required'
        ];
    }
}