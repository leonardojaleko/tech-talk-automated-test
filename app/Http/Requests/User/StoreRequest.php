<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest; 
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest{
    public function authorize(){
        return true;
    }

    public function rules(){ 
        return [
            'name' => 'required|min:4',
            // test tech talk 3
            'email' => ['required', Rule::unique('users')],
            'password' => 'required'
        ];
    }
}
