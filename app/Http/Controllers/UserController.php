<?php

namespace App\Http\Controllers;

use App;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Services\SafeA\UserService as UserSafeAService;

class UserController extends Controller {
    protected $userModel;
    protected $userSafeAService;

    public function __construct(User $userModel, UserSafeAService $userSafeAService) {
        $this->userModel = $userModel;
        $this->userSafeAService = $userSafeAService;
    }

    // test tech talk 1
    // public function store(Request $req) {
    //     return response()->json([], 201);
    // }
    
    // test tech talk 2
    // public function store(StoreRequest $req) {
    //     return response()->json([], 201);
    // }

    // test tech talk 4
    // public function store(StoreRequest $req) {
    //     $user = $this->userModel;
    //     $user->name = $req->name;
    //     $user->email = $req->email;
    //     $user->password = $req->password;
    //     $user->save();

    //     return response()->json($user, 201);
    // }

    // public function update(UpdateRequest $req, $id) {
    //     $user = $this->userModel->find($id);
    //     $user->name = $req->name;
    //     $user->email = $req->email;
    //     $user->save();
    //     return response()->json($user, 200);
    // }

    // public function show($id) {
    //     $user = $this->userModel->find($id);
    //     return response()->json($user, 200);
    // }

    // public function index(Request $req) {
    //     $users = $this->userModel->paginate($req->paginate);
    //     return response()->json($users, 200);
    // }

    // public function destroy($id) {
    //     $user = $this->userModel->find($id);
    //     $user->delete();
    //     return response()->json(1, 200);
    // }

    // public function groups($id) {
    //     $user = $this->userModel->whereId($id)->with('groups')->first();
    //     return response()->json($user->groups, 200);
    // }

    // public function storeAndCreateSafeA(StoreRequest $req) {
    //     $user = $this->userModel;
    //     $user->name = $req->name;
    //     $user->email = $req->email;
    //     $user->password = $req->password;
    //     $user->save();

    //     $userSafeA = $this->userSafeAService->create($user);

    //     $user['userSafeA'] = $userSafeA;

    //     return response()->json($user, 201);
    // }
}
