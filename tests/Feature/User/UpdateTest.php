<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class UpdateTest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $user = User::factory()->create();

        $response = $this->put('api/users/'.$user->id, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(200);
    }

    public function test_return_validation_error_no_fields_sent() {
        $response = $this->put('api/users/1', []);

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                "name" => [
                    'The name field is required.'
                ],
                "email" => [
                    'The email field is required.'
                ],
                "password" => [
                    'The password field is required.'
                ]
            ],
        ]);
    }

    public function test_return_validation_error_email_already_used() {
        $users = User::factory()->count(2)->create();

        $response = $this->put('api/users/'.$users[0]->id, [
            'name' => $users[1]->name,
            'email' => $users[1]->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                "email" => [
                    'The email has already been taken.'
                ]
            ],
        ]);
    }

    public function test_update_correct() {
        $user = User::factory()->create();
        $userUpdate = User::factory()->make();

        $response = $this->put('api/users/'.$user->id, [
            'name' => $userUpdate->name,
            'email' => $userUpdate->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'name' => $userUpdate->name,
            'email' => $userUpdate->email,
        ]);
    }
}
