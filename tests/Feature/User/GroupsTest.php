<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\User;
use App\Models\Group;

class GroupsTest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $user = User::factory()->create();

        $response = $this->get('api/users/'.$user->id.'/groups');

        $response->assertStatus(200);
    }

    public function test_return_groups() {
        $user = User::factory()->has(Group::factory()->count(5), 'groups')->create(); 
        
        $response = $this->get('api/users/'.$user->id.'/groups');
        
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(5)
                ->first(fn ($json) =>
                    $json->where('id', $user->groups[0]->id)
                        ->where('name', $user->groups[0]->name)
                        ->etc()
                )
        );
    }
}
