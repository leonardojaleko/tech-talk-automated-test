<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class ShowTest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $user = User::factory()->create();

        $response = $this->get('api/users/'.$user->id);

        $response->assertStatus(200);
    }

    public function test_return_object() {
        $user = User::factory()->create();

        $response = $this->get('api/users/'.$user->id);

        $response->assertStatus(200);
        $response->assertJson([
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }
}
