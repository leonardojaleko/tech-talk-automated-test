<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class IndexTest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $response = $this->get('api/users/');
        $response->assertStatus(200);
    }

    public function test_return_pagination() {
        $user = User::factory()->count(30)->create();

        $response = $this->get('api/users?paginate=5&page=2');

        $responseArray = json_decode($response->getContent());
        
        $response->assertStatus(200);
        $response->assertJson([
            'per_page' => 5,
            'current_page' => 2,
            'total' => 30
        ]);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'name',
                    'email'
                ]
            ]
        ]);
    }
}
