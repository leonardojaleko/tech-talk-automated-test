<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class StoreTest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $user = User::factory()->make();

        $response = $this->post('api/users', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(201);
    }

    public function test_return_validation_error_no_fields_sent() {
        $response = $this->post('api/users', []);

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                "name" => [
                    'The name field is required.'
                ],
                "email" => [
                    'The email field is required.'
                ],
                "password" => [
                    'The password field is required.'
                ]
            ],
        ]);
    }

    public function test_return_validation_error_email_already_used() {
        $user = User::factory()->create();

        $response = $this->post('api/users', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                "email" => [
                    'The email has already been taken.'
                ]
            ],
        ]);
    }

    public function test_store_correct() {
        $user = User::factory()->make();

        $response = $this->post('api/users', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(201);

        $response->assertJson([
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }
}
