<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\SafeA\UserService;
use Mockery\MockInterface;
use Tests\TestCase;
use App\Models\User;
use Mockery;

class StoreAndCreateSafeATest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $user = User::factory()->make();

        // test tech talk 5
        // $this->instance(
        //     UserService::class,
        //     Mockery::mock(UserService::class, function (MockInterface $mock) use($user) {
        //         $mock->shouldReceive('create')->once()->andReturn([
        //             'id' => '123',
        //             'externalId' => 1,
        //             'name' => $user->name
        //         ]);
        //     })
        // );

        $response = $this->post('api/users/safe-a', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678'
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'name' => $user->name,
            'email' => $user->email,
            'userSafeA' => [
                'id' => '123',
                'externalId' => $response->original['id'],
                'name' => $user->name
            ]
        ]);
    }
}
