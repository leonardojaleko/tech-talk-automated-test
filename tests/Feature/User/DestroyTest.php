<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class DestroyTest extends TestCase {
    use RefreshDatabase;

    public function test_route_exists() {
        $user = User::factory()->create();

        $response = $this->delete('api/users/'.$user->id);

        $response->assertStatus(200);
    }

    public function test_destroy_correct() {
        $user = User::factory()->create();

        $response = $this->delete('api/users/'.$user->id);

        $response->assertStatus(200);
        $this->assertEquals(null, User::find($user->id));
    }
}
